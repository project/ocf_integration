# Open Collective Foundation Integration

This module was written to facilitate the use of the Open collective 
Foundation (OCF) as fiscal sponsor for a Drupal Camp or similar event with a registration and paid ticket process.

The OCF portal has a way to create an event with paid tickets, but no easy 
way to collect all the information we need to go along with a camp 
registration (e.g. contact email, t-shirt size, food preference, vaccine 
attestation, etc).

For the event we are using a webform as the mechanism to register people 
and then sending them to the ticket purchase page on the OCF site to 
complete the transaction.

However, this leave a gap where we have to manually reconcile the 
registrations with the actual payments to see if we are missing any. 
Worse yet, the ticket payment allows checkout with the name "Guest" and 
doesn't provide to us the email address used.

To help close the gap, this module uses a mechanism to connect a ticket 
purchase on the OCF site with the webform registration submission. By 
constructing a URL containing the webform submission ID as a tag for the 
ticket order, we are able to use OCF webhook data and also user navigation 
back to our site following the link provided as a "redirect" to confirm the 
payment and link a specific ticket order to the registration.

If you want to contribute or do something similar with a webhook, take a 
look at this tool to help you grab the data: 
https://www.drupal.org/project/request_dumper

Other integration points may be added over time, such as using the OCF 
graphQL api.

## The Recipe With Redirects

This module by itself is not useful. You need to have a webform created for
event registration. An example is in `config/example`

This example webform uses a "regcode" in the query string to toggle on a
free registration for e.g. volunteers or sponsor staff.

In this example you need to use the redirect module since the free ticket
registrations should not be sent over to OCF.

The confirmation URL in the webform is a key part of the recipe.

The ticket values in the example webform are "general-admission" and 
"individual-sponsor"

The confirmation URL is:

`/2023-registration-[webform_submission:values:ticket:raw]?name=[webform_submission:values:name:rawurlencode]&legalName=[webform_submission:values:name:rawurlencode]&email=[webform_submission:values:email:rawurlencode]&tags=sid-[webform_submission:sid]&redirect=[site:url:rawurlencode]record-payment/[webform_submission:sid]`

Breaking it down, you can example the path:

`/2023-registration-[webform_submission:values:ticket:raw]`

Then look at the query parameters which are passing information to OCF:

`name=[webform_submission:values:name:rawurlencode]`

`legalName=[webform_submission:values:name:rawurlencode]`

`email=[webform_submission:values:email:rawurlencode]`

`tags=sid-[webform_submission:sid]`

`redirect=[site:url:rawurlencode]record-payment/[webform_submission:sid]`

The "tags" parameter is the most important in terms of integrating with 
this module.

The redirects are set to pass through query string parameters and these paths:

`/2023-registration-general-admission`

`/2023-registration-individual-sponsor`

should redirect to each OCF event ticket of the right tier, e.g.:

`https://opencollective.com/MYCOLLECTIVE/events/MYEVENT/order/148608`

An advantage of this is that if you are changing prices (e.g. early to
regular).  You can change the tier in the redirect without changing the
webform (though you may also need to change the form for the dollar
amount shown).

For the "free" registration, there is no ticket value so the last redirect is
from:

`/2023-registration-`

to a landing page like:

`/thanks-registering-volunteer`

## The Recipe Without Redirects

The recipe without redirects would assume that you have a different
mechanism or webform for handling free registrations OR possibly
you have an OCF event ticket for $0 that you want those people
to register for.

In that case, you would use the ticket ID (the number at the end of the
ticker order URL) as the ticket value in the webform, and set the
webform confirmation URL to redirect the user 
