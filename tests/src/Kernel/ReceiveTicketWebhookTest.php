<?php

namespace Drupal\Tests\ocf_integration\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\ocf_integration\Controller\ReceiveTicketWebhook;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformInterface;
use Drupal\webform\Entity\Webform;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test receive ticket webhook.
 *
 * @group ocf_integration
 */
class ReceiveTicketWebhookTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'webform',
    'ocf_integration',
  ];

  /**
   * The webform being tested.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected WebformInterface $webform;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installSchema('webform', ['webform']);
    $this->installEntitySchema('webform_submission');
    $this->installEntitySchema('ocf_ticket_webhook');
    $this->installConfig(static::$modules);
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = Webform::create(['id' => 'registration_test', 'title' => 'Test']);
    $elements = [
      'name' => [
        '#type' => 'textfield',
        '#title' => 'Name',
      ],
      'ticket' => [
        '#type' => 'select',
        '#title' => 'Ticket level',
        '#options' => [
          'standard' => 'Standard',
          'sponsor' => 'Individual Sponsor',
        ],
      ],
    ];
    $webform->setElements($elements);
    $webform->save();
    $this->webform = Webform::load($webform->id());
  }

  /**
   * Request data test cases.
   *
   * @dataProvider provideRequestData
   */
  public function testWebhookRequest(string $request_content, $saved_value): void {
    $values = [
      'sid' => 7778,
      'webform_id' => $this->webform->id(),
      'data' => ['name' => 'John Smith', 'ticket' => 'standard'],
    ];
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = WebformSubmission::create($values);
    $webform_submission->save();
    self::assertEquals(7778, $webform_submission->id());
    $request = Request::create('/receive-ticket-webhook/foo', 'POST', [], [], [], [], $request_content);
    $controller = ReceiveTicketWebhook::create($this->container);
    $response = $controller->webhook($request);
    self::assertEquals(200, $response->getStatusCode());
    $storage = $this->entityTypeManager->getStorage('ocf_ticket_webhook');
    $saved = $storage->loadByProperties();
    self::assertEquals($saved_value, !empty($saved));
    if ($saved) {
      $entity = reset($saved);
      self::assertEquals($webform_submission->id(), $entity->get('webform')->target_id);
      self::assertEquals("Afsfdsfds Tyyyy", $entity->get('name')->getString());
      self::assertEquals("617000", $entity->get('order_id')->getString());
    }
  }

  /**
   * Data provider.
   *
   * @return array[]
   *   Request data and boolean true a ocf_ticket_webhook should be saved.
   */
  public static function provideRequestData(): array {
    return [
      [
        '{"createdAt":"2023-01-25T13:39:19.201Z","id":4927620,"type":"ticket.confirmed","CollectiveId":574860,"data":{"recipient":{"name":"Afsfdsfds Tyyyy"},"tier":{"id":51052,"name":"Standard","amount":1000,"currency":"USD","description":"","maxQuantity":null,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00"},"order":{"id":617000,"totalAmount":1000,"currency":"USD","description":"","tags":["sid-7778"],"interval":"month","createdAt":"2023-01-25T13:39:19.179Z","quantity":1,"FromCollectiveId":602041,"TierId":51052,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00 / month"}}}',
        TRUE,
      ],
      [
        '{"createdAt":"2023-01-25T13:39:19.201Z","id":4927620,"type":"ticket.confirmed","CollectiveId":574860,"data":{"recipient":{"name":"Afsfdsfds Tyyyy"},"tier":{"id":51052,"name":"Standard","amount":1000,"currency":"USD","description":"","maxQuantity":null,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00"},"order":{"id":617000,"totalAmount":1000,"currency":"USD","description":"","tags":["sid:7778"],"interval":"month","createdAt":"2023-01-25T13:39:19.179Z","quantity":1,"FromCollectiveId":602041,"TierId":51052,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00 / month"}}}',
        TRUE,
      ],
      [
        '{"createdAt":"2023-01-25T13:39:19.201Z","id":4927620,"type":"ticket.confirmed","CollectiveId":574860,"data":{"recipient":{"name":"Afsfdsfds Tyyyy"},"tier":{"id":51052,"name":"Standard","amount":1000,"currency":"USD","description":"","maxQuantity":null,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00"},"order":{"id":617000,"totalAmount":1000,"currency":"USD","description":"","tags":["garbage"],"interval":"month","createdAt":"2023-01-25T13:39:19.179Z","quantity":1,"FromCollectiveId":602041,"TierId":51052,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00 / month"}}}',
        FALSE,
      ],
      [
        '{"createdAt":"2023-01-25T13:39:19.201Z","id":4927620,"type":"garbage","CollectiveId":574860,"data":{"recipient":{"name":"Afsfdsfds Tyyyy"},"tier":{"id":51052,"name":"Standard","amount":1000,"currency":"USD","description":"","maxQuantity":null,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00"},"order":{"id":617000,"totalAmount":1000,"currency":"USD","description":"","tags":["sid-7778"],"interval":"month","createdAt":"2023-01-25T13:39:19.179Z","quantity":1,"FromCollectiveId":602041,"TierId":51052,"formattedAmount":"$10.00","formattedAmountWithInterval":"$10.00 / month"}}}',
        FALSE,
      ],
    ];
  }

}
