<?php

namespace Drupal\ocf_integration;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Ticket webhook entities.
 */
class TicketWebhookListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('Entity ID');
    $header['label'] = $this->t('Name');
    $header['order_id'] = $this->t('Order Id');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\ocf_integration\Entity\TicketWebhook $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['order_id'] = Link::createFromRoute(
      $entity->get('order_id')->getString(),
      'entity.ocf_ticket_webhook.canonical',
      ['ocf_ticket_webhook' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
