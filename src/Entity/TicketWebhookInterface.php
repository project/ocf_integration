<?php

namespace Drupal\ocf_integration\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining  Ticket webhook entities.
 */
interface TicketWebhookInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the Registration order confirmation creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Registration order confirmation.
   */
  public function getCreatedTime();

  /**
   * Sets the Registration order confirmation creation timestamp.
   *
   * @param int $timestamp
   *   The Registration order confirmation creation timestamp.
   *
   * @return \Drupal\ocf_integration\Entity\TicketWebhookInterface
   *   The called Registration order confirmation entity.
   */
  public function setCreatedTime($timestamp);

}
