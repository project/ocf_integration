<?php

namespace Drupal\ocf_integration\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Ticket Webhook entity.
 *
 * @ingroup ocf_integration
 *
 * @ContentEntityType(
 *   id = "ocf_ticket_webhook",
 *   label = @Translation("Ticket webhook data"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ocf_integration\TicketWebhookListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ocf_integration\Form\TicketWebhookForm",
 *       "add" = "Drupal\ocf_integration\Form\TicketWebhookForm",
 *       "edit" = "Drupal\ocf_integration\Form\TicketWebhookForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ocf_integration\TicketWebhookAccessControlHandler",
 *   },
 *   base_table = "ocf_ticket_webhook",
 *   translatable = FALSE,
 *   admin_permission = "administer ocf_ticket_webhook entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ocf_ticket_webhook/{ocf_ticket_webhook}",
 *     "add-form" = "/admin/structure/ocf_ticket_webhook/add",
 *     "edit-form" = "/admin/structure/ocf_ticket_webhook/{ocf_ticket_webhook}/edit",
 *     "delete-form" = "/admin/structure/ocf_ticket_webhook/{ocf_ticket_webhook}/delete",
 *     "collection" = "/admin/structure/ocf_ticket_webhook",
 *   },
 *   field_ui_base_route = "entity.ocf_ticket_webhook.collection"
 * )
 */
class TicketWebhook extends ContentEntityBase implements TicketWebhookInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['webform'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Webform Submission'))
      ->setSetting('target_type', 'webform_submission')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniqueField', [])
      ->setRequired(TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Order ID'))
      ->setDescription(t('The ID of the order.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Webhook type'))
      ->setDescription(new TranslatableMarkup('The webhook type.'))
      ->setSettings([
        'max_length' => 128,
        'is_ascii' => TRUE,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Purchaser name'))
      ->setDescription(new TranslatableMarkup('The purchaser name.'))
      ->setSettings([
        'max_length' => 1024,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['tier_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Tier ID'))
      ->setDescription(new TranslatableMarkup('The Tier ID (ticket type) of the order.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['total_amount'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Total amount'))
      ->setDescription(new TranslatableMarkup('The amount of the order (in cents).'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['order_created'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Order created timestamp'))
      ->setDescription(new TranslatableMarkup('The time of the order.'))
      ->setSetting('unsigned', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['json_data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('JSON data'))
      ->setDescription(new TranslatableMarkup('The raw webhook JSON data.'))
      // Makes this a BLOB field.
      ->setSetting('case_sensitive', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    return $fields;
  }

}
