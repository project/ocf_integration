<?php

namespace Drupal\ocf_integration\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Registration order confirmation entity.
 *
 * @ingroup ocf_integration
 *
 * @ContentEntityType(
 *   id = "ocf_order_confirmation",
 *   label = @Translation("Registration order confirmation"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ocf_integration\RegistrationOrderConfirmationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\ocf_integration\Form\RegistrationOrderConfirmationForm",
 *       "add" = "Drupal\ocf_integration\Form\RegistrationOrderConfirmationForm",
 *       "edit" = "Drupal\ocf_integration\Form\RegistrationOrderConfirmationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ocf_integration\RegistrationOrderConfirmationAccessControlHandler",
 *   },
 *   base_table = "ocf_order_confirmation",
 *   translatable = FALSE,
 *   admin_permission = "administer ocf_order_confirmation entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "order_idv2",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ocf_order_confirmation/{ocf_order_confirmation}",
 *     "add-form" = "/admin/structure/ocf_order_confirmation/add",
 *     "edit-form" = "/admin/structure/ocf_order_confirmation/{ocf_order_confirmation}/edit",
 *     "delete-form" = "/admin/structure/ocf_order_confirmation/{ocf_order_confirmation}/delete",
 *     "collection" = "/admin/structure/ocf_order_confirmation",
 *   },
 *   field_ui_base_route = "entity.ocf_order_confirmation.collection"
 * )
 */
class RegistrationOrderConfirmation extends ContentEntityBase implements RegistrationOrderConfirmationInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['webform'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Webform Submission'))
      ->setSetting('target_type', 'webform_submission')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniqueField', [])
      ->setRequired(TRUE);

    $fields['order_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Order ID'))
      ->setDescription(new TranslatableMarkup('The ID of the order.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['order_idv2'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Order ID v2'))
      ->setDescription(new TranslatableMarkup('The OCF order ID v2.'))
      ->setSettings([
        'max_length' => 128,
        'is_ascii' => TRUE,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Registration order confirmation is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    return $fields;
  }

}
