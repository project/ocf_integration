<?php

namespace Drupal\ocf_integration;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Registration order confirmation entities.
 */
class RegistrationOrderConfirmationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('Entity ID');
    $header['label'] = $this->t('Order Id');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\ocf_integration\Entity\RegistrationOrderConfirmation $entity */
    $row['id'] = $entity->id();
    $row['label'] = Link::createFromRoute(
      $entity->label(),
      'entity.ocf_order_confirmation.canonical',
      ['ocf_order_confirmation' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
