<?php

namespace Drupal\ocf_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * OCF Integration configuration form.
 */
class OcfConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ocf_integration.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ocf_integration_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ocf_integration.configuration');
    $expected_token = $config->get('webhook_token', '');
    $route_params = ['token' => $expected_token];
    $options['absolute'] = TRUE;
    $form['token'] = [
      '#type' => 'item',
      '#title' => $this->t('Webhook path'),
      '#markup' => Url::fromRoute('ocf_integration.receive_ticket_webhook', $route_params, $options)->toString(),
    ];
    $form['redirect_path'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Redirect Path'),
      '#description' => $this->t('The path to redirect after registering an order.'),
      '#default_value' => $config->get('redirect_path'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $path = $form_state->getValue('redirect_path');
    if ($path[0] !== '/') {
      $form_state->setErrorByName('redirect_path', $this->t('Redirect path must start with /'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Ensure there is only 1 leading slash.
    $path = '/' . ltrim($form_state->getValue('redirect_path'), '/');
    // Store the config.
    $this->config('ocf_integration.configuration')
      ->set('redirect_path', $path)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
