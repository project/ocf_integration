<?php

namespace Drupal\ocf_integration\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Registration order confirmation edit forms.
 */
class RegistrationOrderConfirmationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Registration order confirmation.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Registration order confirmation.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ocf_order_confirmation.canonical', ['ocf_order_confirmation' => $entity->id()]);
  }

}
