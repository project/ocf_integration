<?php

namespace Drupal\ocf_integration\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Ticket webhook edit forms.
 */
class TicketWebhookForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Ticket webhook.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Ticket webhook.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.ocf_ticket_webhook.canonical', ['ocf_ticket_webhook' => $entity->id()]);
  }

}
