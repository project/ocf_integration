<?php

namespace Drupal\ocf_integration;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Registration order confirmation entity.
 *
 * @see \Drupal\ocf_integration\Entity\RegistrationOrderConfirmation.
 */
class RegistrationOrderConfirmationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($admin_permission = $this->entityType->getAdminPermission()) {
      $access = AccessResult::allowedIfHasPermission($account, $admin_permission);
      if ($access->isAllowed()) {
        return $access;
      }
    }
    /** @var \Drupal\ocf_integration\Entity\RegistrationOrderConfirmationInterface $entity */

    switch ($operation) {

      case 'view':
        if ($entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view published ocf_order_confirmation entities');
        }
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
