<?php

namespace Drupal\ocf_integration\Controller;

use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Record a payment when a user returns from buying a Ticket at Open Collective.
 */
class RecordPayment extends ControllerBase {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The module config.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Instance of the entity type manager.
   */
  public function __construct(Config $config, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')->get('ocf_integration.configuration'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to a GET request.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission ID.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Exception
   */
  public function handleSid(WebformSubmissionInterface $webform_submission, Request $request): Response {
    $order_id = $request->get('orderId');
    $order_idv2 = $request->get('orderIdV2');
    $storage = $this->entityTypeManager()->getStorage('ocf_order_confirmation');
    // Don't allow the same webform submission to be used to record multiple.
    $query = $storage->getQuery()->accessCheck(FALSE);
    $existing = $query->condition('webform', $webform_submission->id())->execute();
    // And also don't allow attackers to discover which sid values are valid.
    if (!$order_id || !$order_idv2 || $existing) {
      throw new NotFoundHttpException();
    }
    $props = [
      'webform' => $webform_submission->id(),
      'order_id' => (int) $order_id,
      'order_idv2' => $order_idv2,
      'created' => $request->server->get('REQUEST_TIME'),
      'status' => 1,
    ];
    /** @var \Drupal\ocf_integration\Entity\RegistrationOrderConfirmation $entity */
    $entity = $storage->create($props);
    // We have a valid webform submission ID, so save regardless of validation.
    $entity->save();
    $redirect_path = $this->config->get('redirect_path') ?: '/';
    $options['absolute'] = TRUE;
    return new RedirectResponse(Url::fromUserInput($redirect_path, $options)->toString(), 302);
  }

}
