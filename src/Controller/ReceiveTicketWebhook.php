<?php

namespace Drupal\ocf_integration\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Receive ticket webhook controller.
 */
class ReceiveTicketWebhook extends ControllerBase {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The module config.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Instance of the entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory service.
   */
  public function __construct(Config $config, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
    $this->setLoggerFactory($logger_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')->get('ocf_integration.configuration'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Responds to a POST request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @code
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Exception
   */
  public function webhook(Request $request): Response {
    // Example webhook data:
    // {
    //   "createdAt": "2023-01-22T15:34:49.256Z",
    //   "id": 4912183,
    //   "type": "ticket.confirmed",
    //   "CollectiveId": 574860,
    //   "data": {
    //     "recipient": {
    //       "name": "Dixisset Oratione Dixisset"
    //     },
    //     "tier": {
    //       "id": 51052,
    //       "name": "Test for data",
    //       "amount": 0,
    //       "currency": "USD",
    //       "description": "",
    //       "maxQuantity": null,
    //       "formattedAmount": "$0.00",
    //       "formattedAmountWithInterval": "$0.00"
    //     },
    //     "order": {
    //       "id": 616043,
    //       "totalAmount": 0,
    //       "currency": "USD",
    //       "description": "Monthly financial contribution to DrupalCamp NJ 2023 (Test for data)",
    //       "tags": ["something"],
    //       "interval": "month",
    //       "createdAt": "2023-01-22T15:34:49.234Z",
    //       "quantity": 1,
    //       "FromCollectiveId": 600599,
    //       "TierId": 51052,
    //       "formattedAmount": "$0.00",
    //       "formattedAmountWithInterval": "$0.00 / month"
    //     }
    //   }
    // }.
    $json = $request->getContent();
    $data = json_decode($json, TRUE);
    $type = $data['type'] ?? '';
    if ($type === 'ticket.confirmed') {
      $sid = NULL;
      $tags = $data['data']['order']['tags'] ?? [];
      foreach ($tags as $tag) {
        $parts = preg_split('/[:-]/', $tag, 2, PREG_SPLIT_NO_EMPTY);
        if ($parts[0] === 'sid' && !empty($parts[1]) && ctype_digit($parts[1])) {
          $sid = $parts[1];
        }
      }
      $webform_storge = $this->entityTypeManager()->getStorage('webform_submission');
      $webform_submission = $sid ? $webform_storge->load($sid) : NULL;
      if ($webform_submission) {
        $storage = $this->entityTypeManager()->getStorage('ocf_ticket_webhook');
        $props = [
          'id' => $data['id'] ?? NULL,
          'type' => $data['type'],
          'webform' => $webform_submission->id(),
          'order_id' => $data['data']['order']['id'] ?? NULL,
          'name' => $data['data']['recipient']['name'] ?? NULL,
          'tier_id' => $data['data']['tier']['id'] ?? NULL,
          'total_amount' => (int) ($data['data']['order']['totalAmount'] ?? 0),
          'order_created' => (int) strtotime($data['data']['order']['createdAt'] ?? NULL),
          'json_data' => $json,
        ];
        /** @var \Drupal\ocf_integration\Entity\TicketWebhook $entity */
        $entity = $storage->create($props);
        $entity->save();
      }
      else {
        $this->getLogger('ocf_integration')->warning("No sid found in webhook data: \n@json", ['@json' => $json]);
      }
    }
    $ret = ["result" => "success"];
    return new JsonResponse($ret);
  }

  /**
   * Access check.
   *
   * @param string $token
   *   The token in the path.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(string $token): AccessResult {
    $expected_token = $this->config->get('webhook_token', '');
    $access = $token && $expected_token;
    return AccessResult::allowedIf($access && hash_equals($expected_token, $token));
  }

}
